'use strict'

module.exports = function(lenght,width) {
	
	return {
		getArea : getArea,
		getPerimeter : getPerimeter,
	};

	function getArea () {
		return lenght*width;
	}
	function getPerimeter(){
		return 2*lenght*width;
	}
}