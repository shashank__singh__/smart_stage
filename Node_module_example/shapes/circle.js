'use strict'

module.exports = function(radius) {
	
	return {
		getArea : getArea,
		getPerimeter : getPerimeter,
	};

	function getArea () {
		return 3.14*radius*radius;
	}
	function getPerimeter(){
		return 2*3.14*radius;
	}
}