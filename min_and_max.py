__author__ = 'shas'
import functools

list1={45098,0,-1,4,5,-10,6,7, 8,9,43,658,352,53,-10}
list2=list1.copy()
max=functools.reduce(lambda x,y: x if (x>y) else y,list1)
min=functools.reduce(lambda x,y : x if(x<y) else y,list1)
#print(min)
#print(max)

def minimum(list):
    mini=list.pop()
    def main(mini,list):
        element=list.pop()
        if mini>element:
            mini=element
        if (len(list)==1):
            return mini
        else:
            return main(mini,list)

    return main(mini,list)

def maximum(list2):
    max=list2.pop()
    def main(max,list2):
        element=list2.pop()
        if max<element:
            max=element
        if (len(list2)==1):
            return max
        else:
            return main(max,list2)

    return main(max,list2)
print(minimum(list1))
print(maximum(list2))