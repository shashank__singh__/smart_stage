'use strict'

var circle = require('./shapes/circle.js')(10);

console.log('area ', circle.getArea());
console.log('perimeter ', circle.getPerimeter());

var rectangle = require('./shapes/rectangle.js')(20,10.33485973);

console.log('area ', rectangle.getArea());
console.log('perimeter ', rectangle.getPerimeter());